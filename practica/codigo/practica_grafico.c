#include <time.h> // Me da la funcion clock() y la constante CLOCKS_PER_SEC
#include <stdio.h> // Ya que lo hice en C le agrego un printf para probarlo
#define EJECUTAR_MS 59 // Limito la cantidad de ms que corre el programa

long tiempo_us(){
	  return (clock() * 1000 * 1000 / CLOCKS_PER_SEC); // Transformo el ciclo de reloj actual, en tiempo de ejecucion del programa a microsegundos (uso micro en lugar de mili porque sino pierde pasos porque demoran los printf)
}

long ms_dibujo = 0; // global
long ms_dibujo_anterior = 0; // global
int dibujado = 0;

void secuencia(int alto_ms, int bajo_ms, long* timer_us, int* estado, int numero){
	if  (numero == 2) dibujado = 0;
	if(*estado == 1){
		if( (dibujado | numero == 2) & ((tiempo_us() - 1000 * alto_ms) > *timer_us) ){
			*estado = 0;
			*timer_us = tiempo_us();
			printf("   _______"); dibujado = 1;
		}else{
			ms_dibujo = tiempo_us()/1000;
			if(dibujado | (ms_dibujo > ms_dibujo_anterior & numero==2)){
				printf("         |"); dibujado = 1;
				if (numero==0) ms_dibujo_anterior = ms_dibujo;
			}
		}
	}
	else{
		if( (dibujado | numero == 2) & ((tiempo_us() - 1000 * bajo_ms) > *timer_us) ){
			*estado = 1;
			*timer_us = tiempo_us();
			printf("   _______"); dibujado = 1; 
		}else{
			ms_dibujo = tiempo_us()/1000;
			if(dibujado | (ms_dibujo > ms_dibujo_anterior & numero==2)){
				printf("   |      "); dibujado = 1;
				if(numero==0) ms_dibujo_anterior = ms_dibujo;
			}
		}
	}
}

void main(void){
	long timer_us[] = {0, 0, 0};
       	int estado[] = {1, 1, 1};
	int alto_ms[] = {10, 15, 20};
	int bajo_ms[] = {10, 15, 15};

	for(int i = 2; i >= 0; i--) printf("   _______");
	printf("\n");

	while(tiempo_us() < 1000 * EJECUTAR_MS){ // Loop principal
		for(int i = 2; i >= 0; i--){
			secuencia(alto_ms[i], bajo_ms[i], &timer_us[i], &estado[i], i); // Control de secuencia 'i'
			if(dibujado==1 && i==0) printf("\n");
		}
	}
}
