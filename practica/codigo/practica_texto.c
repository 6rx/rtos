#include <time.h> // Me da la funcion clock() y la constante CLOCKS_PER_SEC
#include <stdio.h> // Ya que lo hice en C le agrego un printf para probarlo
#define EJECUTAR_MS 100 // Limito la cantidad de ms que corre el programa

long tiempo_us(){
	  return (clock() * 1000 * 1000 / CLOCKS_PER_SEC); // Transformo el ciclo de reloj actual, en tiempo de ejecucion del programa a microsegundos (uso micro en lugar de mili porque sino pierde pasos porque demoran los printf)
}

void secuencia(int alto_ms, int bajo_ms, long* timer_us, int* estado, int numero){
	if(*estado == 1){
		if((tiempo_us() - 1000 * alto_ms) > *timer_us){
			*estado = 0;
			*timer_us = tiempo_us();
			printf("%d ms: B%d Prender\n",(int)*timer_us/1000, numero);
		}
	}
	else{
		if((tiempo_us() - 1000 * bajo_ms) > *timer_us){
			*estado = 1;
			*timer_us = tiempo_us();
			printf("%d ms: B%d Apagar\n",(int)*timer_us/1000, numero);
		}
	}
}

void main(void){
	long timer_us[] = {0, 0, 0};
       	int estado[] = {1, 1, 1};
	int alto_ms[] = {10, 15, 20};
	int bajo_ms[] = {10, 15, 15};

	for(int i = 0; i < 3; i++) printf("0  ms: B%d Prender\n", i);

	while(tiempo_us() < 1000 * EJECUTAR_MS){ // Loop principal
		for(int i = 0; i < 3; i++){
			secuencia(alto_ms[i], bajo_ms[i], &timer_us[i], &estado[i], i); // Control de secuencia 'i'
		}
	}
}
